# HomeDeco

## Introduction

This is an Android application based on ARCore based, using Sceneform library, where users can select furniture from a list and then virtually try out them to see how would it look like in a room/place.

**Technology used**
* Firebase - Database
* Android - Frontend
* ARCore + Sceneform API for AR development 
* Google Poly - 3D objects 

Tested with Google Pixel 3 XL and Pixel 2


**How does it work?**

Open installed app on an Android device. After the splashscreen presentation, you are given a list of furniture objects. You can search them by name using the magnifying glass icon and select as many objects you like and want to test how it looks in your room. After selection is made, click the bottom rounded button to open the AR playground. At the top you can find a horizontally scrollable list formed by the objects you previously selected. Pick the one you want to see and click on the anchor to draw it. You can rotate, resize and move it wherever you like :)


Watch the demo here(click to redirect):

[![Watch the demo here](http://i3.ytimg.com/vi/Rpx3r6cZzDU/hqdefault.jpg)](https://www.youtube.com/watch?v=Rpx3r6cZzDU)

## Installation

**Prerequisite**
* Android Studio (3.1 or later)
* Android Emulator (27.2.9 or later) / Android phone that supports ARCore


Clone the project from here, build and the run it and done :)
